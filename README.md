# frpc-json-poc

**Tech stack**: _emacs, lsp-mode, tide-mode, node.js, next.js, react, yarn-pnp, yarn 3, typescript, typescript-language-server, eslint, prettier, @next/bundle-analyzer, WebSockets, uWebSockets.js, JSON RPC, ajv, jtd_

```frpc-json-poc``` is based on ```next-app-rh```:

https://gitlab.com/ogorod/next-app-rh

It implements a POC of JSON RPC protocol over WebSockets. The back-end is uWebSockets, the front-end is browser.

# Usage instructions

```bash
git clone git@gitlab.com:ogorod/frpc-json-poc.git
cd frpc-json-poc
touch .rh-trusted # required by rh-project-setup() function
yarn
yarn app:build
yarn app:gate-start # starts front-end web server
yarn app:castle-start # starts RPC server
 ```
