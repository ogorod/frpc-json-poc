;; -*- coding: utf-8 -*-

(defcustom next-app-rh-js-back-end 'lsp-mode
  "Recognised input languages."
  :group 'js-interaction
  :type '(choice (const
                  :tag "lsp-mode"
                  'lsp-mode)
                 (const
                  :tag "tide-mode"
                  'tide-mode)))

(cond
 ((eq next-app-rh-js-back-end 'lsp-mode)
  (require 'lsp-javascript))
 ((eq next-app-rh-js-back-end 'lsp-mode)
  (require 'tide-mode)))

(require 'flycheck)

(define-minor-mode next-app-rh-mode
  "next-app-rh project-specific minor mode."
  :lighter " next-app-rh")

(add-to-list 'rm-blacklist " next-app-rh")

;;; next-app-rh
;;; /b/{

(defun next-app-rh-setup-tide ()
  (let ((project-root (expand-file-name (rh-project-get-root))))
    (setq-local tide-tsserver-executable
                (concat project-root
                        ".yarn/sdks/typescript/bin/tsserver"))
    (setq-local tide-tscompiler-executable
                (concat project-root
                        ".yarn/sdks/typescript/bin/tsc"))
    (setq-local flycheck-javascript-eslint-executable
                (concat project-root
                        ".yarn/sdks/eslint/bin/eslint.js"))
    (tide-setup)
    (prettier-mode 1)
    (tide-hl-identifier-mode 1)))

(defun next-app-rh-config-lsp-javascript ()
  (plist-put
   lsp-deps-providers
   :local (list
           :path
           (lambda (path)
             (concat (expand-file-name (rh-project-get-root))
                     ".yarn/sdks/"
                     path))))
  (lsp-dependency 'typescript-language-server
                  '(:local "typescript-language-server/lib/cli.js"))
  (lsp-dependency 'typescript
                  '(:local "typescript/bin/tsserver"))

  (add-hook
   'lsp-after-initialize-hook
   #'next-app-rh-flycheck-add-eslint-next-to-lsp))

(defun next-app-rh-flycheck-add-eslint-next-to-lsp ()
  (when (seq-contains-p '(js2-mode typescript-mode web-mode) major-mode)
    (flycheck-add-next-checker 'lsp 'javascript-eslint)))

(defun next-app-rh-flycheck-after-syntax-check-hook-once ()
  (remove-hook
   'flycheck-after-syntax-check-hook
   #'next-app-rh-flycheck-after-syntax-check-hook-once
   t)
  (flycheck-buffer))

(eval-after-load 'lsp-mode #'next-app-rh-config-lsp-javascript)

(defun next-app-rh-setup ()
  (when buffer-file-name
    (let ((project-root (rh-project-get-root))
          file-rpath ext-js)
      (when project-root
        (setq file-rpath (expand-file-name buffer-file-name project-root))
        (cond
         ;; This is required as tsserver does not work with files in archives
         ((bound-and-true-p archive-subfile-mode)
          (company-mode 1))

         ((or (setq
               ext-js
               (string-match-p "\\.ts\\'\\|\\.tsx\\'\\|\\.js\\'\\|\\.jsx\\'"
                               file-rpath))
              (string-match-p "^#!.*node"
                              (or (save-excursion
                                    (goto-char (point-min))
                                    (thing-at-point 'line t))
                                  "")))

          (when (boundp 'rh-js2-additional-externs)
            (setq-local rh-js2-additional-externs
                        (append rh-js2-additional-externs
                                '("require" "exports" "module" "process"
                                  "__dirname"))))

          (cond
           ((eq next-app-rh-js-back-end 'lsp-mode)
            (setq-local flycheck-idle-change-delay 3)
            (setq-local flycheck-check-syntax-automatically
                        ;; '(save mode-enabled)
                        '(save idle-change mode-enabled))
            (setq-local flycheck-javascript-eslint-executable
                        (concat (expand-file-name project-root)
                                ".yarn/sdks/eslint/bin/eslint.js"))

            (setq-local lsp-enabled-clients '(ts-ls))
            ;; (setq-local lsp-headerline-breadcrumb-enable nil)
            (setq-local lsp-before-save-edits nil)
            (setq-local lsp-modeline-diagnostics-enable nil)
            (add-hook
             'flycheck-after-syntax-check-hook
             #'next-app-rh-flycheck-after-syntax-check-hook-once
             nil t)
            (lsp)
            ;; (lsp-headerline-breadcrumb-mode -1)
            )
           ((eq next-app-rh-js-back-end 'tide-mode)
            ;; tsserver requires non-.ts files to be manually added to the files
            ;; array in tsconfig.json, otherwise the file will be loaded as part
            ;; of an 'inferred project'. This won't be necessary anymore after
            ;; (if) TypeScript allows defining custom file extensions.
            ;; https://github.com/Microsoft/TypeScript/issues/8328
            (unless ext-js (setq-local tide-require-manual-setup t))
            (next-app-rh-setup-tide))
           (t (error "next-app-rh-js-back-end '%s' is not supported"
                     next-app-rh-js-back-end)))))))))

;;; /b/}
