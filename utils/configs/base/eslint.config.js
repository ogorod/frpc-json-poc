// Hey Emacs, this is -*- coding: utf-8 -*-

// https://github.com/alexgorbatchev/eslint-import-resolver-typescript/issues/2

// Inspired by the following resources:
// https://dev.to/robertcoopercode/using-eslint-and-prettier-in-a-typescript-project-53jb
// https://github.com/flycheck/flycheck/issues/514
// https://github.com/cerner/eslint-config-terra

// To consider:
// https://www.npmjs.com/package/@liquid-labs/catalyst-scripts

module.exports = {
  parser: '@typescript-eslint/parser',
  extends: [
    'plugin:@typescript-eslint/recommended',
    'plugin:import/typescript',
    'airbnb',
  ],
  plugins: [
    'import',
  ],
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: 'module',
  },
  rules: {
    // Place to specify ESLint rules. Can be used to overwrite rules specified
    // from the extended configs.
    // e.g. "@typescript-eslint/explicit-function-return-type": "off",
    'max-len': ['error', 80, {
      ignoreUrls: true,
      ignorePattern: '^(?:import|export)\\s.+\\sfrom\\s.+;$',
    }],
    'operator-linebreak': ['error', 'after'],
    // Let tide (or tsc) and js2-mode handle undefined variables
    'no-undef': 'off',
    // 'brace-style': ['warn', 'stroustrup', { allowSingleLine: true }],
    curly: ['warn', 'all'],
    'arrow-body-style': 'off',
    'no-underscore-dangle': ['error', {
      allowAfterThis: true,
      allow: ['__typename'],
    }],
    'lines-between-class-members': [
      'error', 'always', { exceptAfterSingleLine: true },
    ],
    // 'keyword-spacing': ['error', {
    //   overrides: {
    //     catch: { after: false },
    //     for: { after: false },
    //     if: { after: false },
    //     switch: { after: false },
    //     while: { after: false },
    //   },
    // }],
    // 'no-empty-function': ['error', { allow: ['constructors'] }],
    'no-empty-function': 'off',
    '@typescript-eslint/no-empty-function':  ['error', {
      allow: ['constructors']
    }],
    'no-param-reassign': ['error', { props: false }],
    // quotes: ['error', 'single'],
    quotes: ['error', 'single', { avoidEscape: true }],
    'import/prefer-default-export': 'off',
    'import/extensions': ['error', 'ignorePackages', {
      js: 'never',
      jsx: 'never',
      ts: 'never',
      tsx: 'never',
    }],
    'spaced-comment': ['error', 'always', {
      line: {
        markers: ['/'],
      },
    }],
    'max-classes-per-file': 'off',
    indent: 'off',
    '@typescript-eslint/indent': ['warn', 2, {
      flatTernaryExpressions: true,
    }],
    '@typescript-eslint/explicit-member-accessibility': ['warn', {
      accessibility: 'no-public',
    }],
    'no-useless-constructor': 'off',
    '@typescript-eslint/no-useless-constructor': 'error',
    'no-unused-vars': 'off',
    '@typescript-eslint/no-unused-vars': ['warn', {
      ignoreRestSiblings: true,
      argsIgnorePattern: '^_',
    }],
    'object-curly-newline': ['error', { minProperties: 10, consistent: true }],
    '@typescript-eslint/ban-types': ['error', {
      types: { '{}': false },
      extendDefaults: true,
    }],
    '@typescript-eslint/explicit-function-return-type': ['error'],
    'no-use-before-define': 'off',
    '@typescript-eslint/no-use-before-define': ['warn'],
    'no-shadow': 'off',
    '@typescript-eslint/no-shadow': ['error'],
    'no-redeclare': 'off',
    '@typescript-eslint/no-redeclare': ['error'],
  },
  overrides: [
    {
      files: ['*.js', '*.jsx'],
      rules: { '@typescript-eslint/explicit-function-return-type': 'off' },
    },
  ],
  settings: {
    'import/resolver': {
      node: {
        extensions: ['.js', '.jsx', '.ts', '.tsx'],
      },
      typescript: {
        alwaysTryTypes: true,
      },
    },
  },
};
