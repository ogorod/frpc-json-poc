// Hey Emacs, this is -*- coding: utf-8 -*-

import type {
  Call,
  CallController,
  CallId,
  ClientTransportJson,
  Response,
} from '@frpc-json-poc/frpc';

import { PromiseCallback } from '@frpc-json-poc/frpc';

import type {
  SayHelloRequest,
  SayHelloRequestParams,
  SayHelloResponseMessage,
} from '~/ajv/say-hello-packets';

import {
  sayHelloMethod,
  sayHelloResponseJtdValidate,
} from '~/ajv/say-hello-packets';

export class SayHello {
  static method = sayHelloMethod;

  constructor(private _transport: ClientTransportJson) {}

  async call(
    params: SayHelloRequestParams,
    controller?: CallController,
  ): Promise<SayHelloResponseMessage> {
    const callId = this._transport.makeUniqueCallId();

    const request: SayHelloRequest = {
      header: { type: 'request' },
      method: sayHelloMethod,
      id: callId,
      params,
    };

    const data = JSON.stringify(request);

    return new Promise<SayHelloResponseMessage>((resolve, reject) => {
      this._transport.send(data);

      const call: Call = {
        timeout: setTimeout(
          this.timeoutHandler,
          this._transport.callWaitMaxMilliseconds,
          callId,
          reject,
        ),
        promiseCallback: new PromiseCallback(
          this.promiseCallbackHandler,
          resolve,
          reject,
        ),
      };

      if (controller) {
        controller.cancel = (): void => {
          return this.cancelCallbackHandler(callId, reject);
        };
        call.controller = controller;
      }

      this._transport.addPendingCall(callId, call);
    });
  }

  private readonly timeoutHandler = (
    callId: CallId,
    reject: (reason: Error) => void,
  ): void => {
    reject(new Error('SayHello pending RPC timeout.'));

    this._transport.removePendingCall(callId);
  };

  private readonly cancelCallbackHandler = (
    callId: CallId,
    reject: (reason: Error) => void,
  ): void => {
    reject(new Error('SayHello pending RPC has been canceled.'));

    this._transport.removePendingCall(callId);
  };

  private readonly promiseCallbackHandler = (
    response: Response,
    resolve: (value: SayHelloResponseMessage) => void,
    reject: (reason: Error) => void,
  ): void => {
    if (!sayHelloResponseJtdValidate(response)) {
      reject(
        new Error('Received data type is not valid SayHelloResponse object.'),
      );

      return;
    }

    const { error } = response;

    if ((!error && !response.message) || (error && response.message)) {
      reject(new Error('Unrecognised sayHello() response.'));

      return;
    }

    if (error) {
      reject(
        new Error(
          [
            'sayHello() error response. ',
            error.message ? `Error message: "${error.message}". ` : '',
            `Error code: "${error.code}".`,
          ].join(''),
        ),
      );

      return;
    }

    resolve(response.message as SayHelloResponseMessage);
  };
}
