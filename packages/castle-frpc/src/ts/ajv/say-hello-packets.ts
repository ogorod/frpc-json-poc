// Hey Emacs, this is -*- coding: utf-8 -*-

/* eslint-disable camelcase */

import { JTDSchemaType } from 'ajv/dist/jtd';

import type { CallId, FrpcErrorData, PacketHeader } from '@frpc-json-poc/frpc';

import {
  ajv,
  frpcErrorDataJtd,
  idJdt,
  packetHeaderJtd,
} from '@frpc-json-poc/frpc';

// /b/; say_hello
// /b/{

export const sayHelloMethod = 'say_hello';

export interface SayHelloRequestParams {
  my_name: string;
  client_id: string;
}

export interface SayHelloRequest {
  header: PacketHeader;
  method: typeof sayHelloMethod;
  id: CallId;
  params: SayHelloRequestParams;
}

export interface SayHelloResponseMessage {
  hello: string;
}

export interface SayHelloResponse {
  header: PacketHeader;
  id: CallId;
  error?: FrpcErrorData;
  message?: SayHelloResponseMessage;
}

export const sayHelloRequestJtd: JTDSchemaType<SayHelloRequest> = {
  properties: {
    header: packetHeaderJtd,
    method: { enum: [sayHelloMethod] },
    id: idJdt,
    params: {
      properties: {
        my_name: { type: 'string' },
        client_id: { type: 'string' },
      },
    },
  },
};

export const sayHelloRequestJtdValidate = ajv.compile(sayHelloRequestJtd);

export const sayHelloResponseJtd: JTDSchemaType<SayHelloResponse> = {
  properties: {
    header: packetHeaderJtd,
    id: idJdt,
  },
  optionalProperties: {
    error: frpcErrorDataJtd,
    message: {
      properties: {
        hello: { type: 'string' },
      },
    },
  },
};

export const sayHelloResponseJtdValidate = ajv.compile(sayHelloResponseJtd);

// /b/}
