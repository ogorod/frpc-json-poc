// Hey Emacs, this is -*- coding: utf-8 -*-

// prettier-ignore
export type RemoveIndexSignature<T> = {
  [P in keyof T as string extends P
    ? never
    : number extends P
      ? never
      : P]: T[P];
};

export type Ref = string;

export interface Stoppable {
  stop(): void;
}

export type Resources = Map<Ref, Stoppable>;
