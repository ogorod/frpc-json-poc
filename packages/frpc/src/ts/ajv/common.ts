// Hey Emacs, this is -*- coding: utf-8 -*-

/* eslint-disable camelcase */

import Ajv, { JTDSchemaType } from 'ajv/dist/jtd';

// /b/; ajv jtd
// /b/{

// options can be passed, e.g. { allErrors: true }
export const ajv = new Ajv();

// /b/}

// /b/; Common Errors
// /b/{

// See https://www.jsonrpc.org/specification for error codes
export enum FrpcErrorCode {
  // Invalid FBS was received by the server.
  // An error occurred on the server while extraction the FBS.
  parse_error = -32700,

  // The FBS sent is not a valid Request object.
  invalid_request = -32600,

  // The method does not exist / is not available.
  method_not_found = -32601,

  // Invalid method parameter(s).
  invalid_params = -32602,

  // Internal fRPC error.
  internal_error = -32603,
}

export const frpcErrorMessage = (code: number): string => {
  if (code === FrpcErrorCode.parse_error) {
    return [
      'Invalid FBS was received by the server.',
      'An error occurred on the server while extraction the FBS.',
    ].join(' ');
  }
  if (code === FrpcErrorCode.invalid_request) {
    return 'The FBS sent is not a valid Request object.';
  }
  if (code === FrpcErrorCode.method_not_found) {
    return 'The method does not exist / is not available.';
  }
  if (code === FrpcErrorCode.invalid_params) {
    return 'Invalid method parameter(s).';
  }
  if (code === FrpcErrorCode.internal_error) {
    return 'Internal fRPC error.';
  }
  return 'Unknown error';
};

export interface FrpcErrorData {
  code: number;
  message: string;
}

export class FrpcErrorT extends Error {
  constructor(public code: number, message: string) {
    super(message);
  }
}

// /b/}

// /b/; fRPC Common Packets
// /b/{

export type CallId = number;
export type Method = string;
export type Data = string;

const packetTypeArray = [
  'request',
  'response',
  'multicast',
  'stream',
  'endOfStream',
] as const;

type Mutable<T> = {
  -readonly [K in keyof T]: Mutable<T[K]>;
};

export type PacketTypeTuple = Mutable<typeof packetTypeArray>;
export type PacketType = PacketTypeTuple[number];

export interface PacketHeader {
  type: PacketType;
}

export interface Packet {
  header: PacketHeader;
}

export interface Request {
  header: PacketHeader;
  method: Method;
  id: CallId;
}

export interface Response {
  header: PacketHeader;
  id: CallId;
}

export const idJdt: JTDSchemaType<CallId> = { type: 'uint32' };

export const packetHeaderJtd: JTDSchemaType<PacketHeader> = {
  properties: {
    type: {
      enum: packetTypeArray as PacketTypeTuple,
    },
  },
};

export const frpcErrorDataJtd: JTDSchemaType<FrpcErrorData> = {
  properties: {
    code: { type: 'int32' },
    message: { type: 'string' },
  },
};

export const requestJtd: JTDSchemaType<Request> = {
  properties: {
    header: packetHeaderJtd,
    method: { type: 'string' },
    id: idJdt,
  },
  additionalProperties: true,
};

export const requestJtdValidate = ajv.compile(requestJtd);

export const responseJtd: JTDSchemaType<Response> = {
  properties: {
    header: packetHeaderJtd,
    id: idJdt,
  },
  additionalProperties: true,
};

// /b/}

// /b/; fRPC Typescript client call primitives
// /b/{

export interface CallbackInterface {
  run: (response: Response) => void;
}

export interface CallController {
  cancel?: () => void;
}

export interface Call {
  timeout: ReturnType<typeof setTimeout>;
  controller?: CallController;
  promiseCallback: CallbackInterface;
}

export class PromiseCallback<Value, Reason> implements CallbackInterface {
  constructor(
    private _runHandler: (
      response: Response,
      resolve: (value: Value) => void,
      reject: (reason: Reason) => void,
    ) => void,
    private _resolve: (value: Value) => void,
    private _reject: (reason: Reason) => void,
  ) {}

  run(response: Response): void {
    this._runHandler(response, this._resolve, this._reject);
  }
}

export type Calls = Record<CallId, Call>;

// export interface RpcCall<Params, Message> {
//   call(
//     params: Params,
//     controller?: CallController,
//   ): Promise<Message>;
// }

export type RpcCalls = Record<Method, unknown>;

export interface ClientTransportJson {
  callWaitMaxMilliseconds: number;
  makeUniqueCallId(): CallId;
  send(data: Data): void;
  addPendingCall(callId: CallId, call: Call): void;
  getPendingCall(callId: CallId): Call | undefined;
  removePendingCall(callId: CallId): void;
}

// /b/}
