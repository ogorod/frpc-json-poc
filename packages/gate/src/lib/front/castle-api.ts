// Hey Emacs, this is -*- coding: utf-8 -*-

/* eslint-disable camelcase */
import WebSocket from 'isomorphic-ws';
import { Observable } from 'rxjs';

import type {
  Call,
  CallId,
  Calls,
  ClientTransportJson,
  Data,
  Method,
  Packet,
  Response,
  RpcCalls,
} from '@frpc-json-poc/frpc';

import { description } from '~/lib/front/websocket-event-codes';

export class CastleApi implements ClientTransportJson {
  readonly callWaitMaxMilliseconds = 10 * 1000;

  constructor(wsUrl?: string) {
    if (wsUrl) {
      this.connect(wsUrl);
    }
  }

  // /b/; operations
  // /b/{

  get connected(): boolean {
    return this._ws.readyState === WebSocket.OPEN;
  }

  connect(wsUrl: string): void {
    this._ws = new WebSocket(wsUrl);
    this._ws.addEventListener('open', this.onOpen);

    this._ws.addEventListener('close', (event) => {
      console.info(
        [
          'Castle WebSocket connection closed:',
          description(event.code, event.reason),
        ].join(' '),
      );

      if (!event.wasClean) {
        setTimeout(() => {
          console.info('Attempting to re-connect to Castle WebSocket');
          this.connect(wsUrl);
        }, 5000);
      }
    });
  }

  disconnect(): void {
    this._ws.close();
    this._ws = null as unknown as WebSocket;
  }

  send(data: Data): void {
    this._ws.send(data);
  }

  readonly connected$ = new Observable<boolean>((subscriber) => {
    const openHandler = (): void => {
      subscriber.next(true);
    };

    const closeHandler = (): void => {
      subscriber.next(false);
    };

    this._ws.addEventListener('open', openHandler);
    this._ws.addEventListener('close', closeHandler);

    return (): void => {
      this._ws.removeEventListener('open', openHandler);
      this._ws.removeEventListener('close', closeHandler);
    };
  });

  // /b/}

  private readonly onOpen = (): void => {
    console.info('Connected to Castle WebSocket');
    this._ws.onmessage = this.onMessage;
  };

  private readonly onMessage = (event: WebSocket.MessageEvent): void => {
    if (typeof event.data !== 'string' /* || data instanceof String */) {
      console.warn('Received data type is not string.');
      return;
    }

    let packet: Packet | undefined;
    try {
      packet = JSON.parse(event.data);
    } catch (error) {
      if (error instanceof SyntaxError) {
        console.warn('Received data is not valid JSON string.');
        return;
      }

      throw error;
    }

    if (!packet?.header?.type) {
      console.warn('Received data object header is invalid.');
      return;
    }

    const packetType = packet.header.type;

    if (packetType === 'response') {
      const response = packet as Response;
      if (!response?.id) {
        console.warn('Received response packet id is invalid.');
        return;
      }

      const { id: callId } = response;

      const pendingCall = this.getPendingCall(callId);
      if (pendingCall !== undefined) {
        clearTimeout(pendingCall.timeout);
        if (pendingCall.controller) {
          pendingCall.controller.cancel = undefined;
        }
        this.removePendingCall(callId);
        pendingCall.promiseCallback.run(response);
      } else {
        console.warn(
          [
            `There is no fRPC call with id == ${callId}.`,
            `It could be that the call with id == ${callId} has been,`,
            'cancelled expired after timeout,',
            'or there is a bug in fRPC server.',
          ].join(' '),
        );
      }
    } else {
      console.warn(
        [
          `Received packet header type == ${packetType}.`,
          'Only "response" packet types',
          'are supported by this fRPC client.',
        ].join(' '),
      );
    }
  };

  makeUniqueCallId(): CallId {
    if (this._freeCallIds.length > 0) {
      return this._freeCallIds.shift() as CallId;
    }

    const currentIdCounter = this._idCounter[0];
    do {
      this._idCounter[0] += 1;
      if (currentIdCounter === this._idCounter[0]) {
        throw new RangeError(
          [
            'The number of pending calls exceeded 2^32!',
            'It could be that fRPC library is',
            'insufficient for the required load;',
            'or there is a memory leak somewhere.',
          ].join(' '),
        );
      }
    } while (this._idCounter[0] in this._pendingCalls);

    return this._idCounter[0];
  }

  // makeUniqueCallId(): CallId {
  //   this._idCounter[0] += 1;

  //   if(this._idCounter[0] === 0) {
  //     this._idCounter[0] += 1;
  //     if(this._idCounter[0] in this._pendingCalls) {
  //       // TODO: How are we are going to handle client-side errors?
  //       console.log([
  //         'fRPC calls id counter overflow!',
  //       ].join(' '));
  //     }
  //   }

  //   return this._idCounter[0];
  // }

  addPendingCall(callId: CallId, call: Call): void {
    this._pendingCalls[callId] = call;
  }

  getPendingCall(callId: CallId): Call | undefined {
    return this._pendingCalls[callId];
  }

  removePendingCall(callId: CallId): void {
    this._freeCallIds.push(callId);
    delete this._pendingCalls[callId];
  }

  // clearPendingCallTimeout(callId: CallId): void {
  //   const pendingCall = this._pendingCalls[callId];
  //   if(pendingCall && pendingCall.timeout !== undefined) {
  //     clearTimeout(pendingCall.timeout);

  //     // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
  //     pendingCall.timeout = undefined!;
  //   }
  // }

  // rpcCall<RpcCallClassType >(
  //   RpcCallClass: RpcCallClassType,
  // ): InstanceType<RpcCallClassType> {
  //   let rpcCall = this._rpcCalls[RpcCallClass.method] as
  //     InstanceType<RpcCallClassType> | undefined;
  //   if(rpcCall !== undefined) {
  //     return rpcCall;
  //   }

  //   rpcCall = new RpcCallClass();
  //   this._rpcCalls[RpcCallClass.method] = rpcCall;
  //   return rpcCall;
  // }

  ep<RpcEndPoint>(RpcEndPointClass: {
    method: Method;
    new (api: CastleApi): RpcEndPoint;
  }): RpcEndPoint {
    let rpcCall = this._rpcCalls[RpcEndPointClass.method] as RpcEndPoint;
    if (rpcCall !== undefined) {
      return rpcCall;
    }

    rpcCall = new RpcEndPointClass(this);
    this._rpcCalls[RpcEndPointClass.method] = rpcCall;
    return rpcCall;
  }

  private _ws: WebSocket = null as unknown as WebSocket;

  private _pendingCalls: Calls = {};

  private _idCounter = new Uint32Array(1);
  private _freeCallIds: CallId[] = [];

  private _rpcCalls: RpcCalls = {};
}
