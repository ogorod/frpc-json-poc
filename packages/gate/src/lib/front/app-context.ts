// Hey Emacs, this is -*- coding: utf-8 -*-

import { createContext, useContext } from 'react';

import { inBrowser } from '@frpc-json-poc/iso-share';

import { CastleApi } from '~/lib/front/castle-api';

export class AppContextValue {
  // readonly castleApi = new CastleApi('ws://localhost:9001');
  readonly castleApi = new CastleApi(
    inBrowser() ? 'ws://localhost:9001' : undefined,
  );

  readonly xxx = 'xxx';
}

export const AppContext = createContext<AppContextValue>(
  null as unknown as AppContextValue,
);

export const useAppContext = (): AppContextValue => useContext(AppContext);
