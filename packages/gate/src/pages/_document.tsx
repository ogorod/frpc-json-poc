// Hey Emacs, this is -*- coding: utf-8 -*-

// https://dev.to/adambaialiev/how-to-add-styled-components-to-next-js-project-using-typescript-2nl4
// https://dev.to/rffaguiar/nextjs-typescript-styled-components-1i3m
// https://github.com/manakuro/nextjs-styled-component-material-ui-example/blob/master/pages/_document.js
// https://github.com/mui-org/material-ui/blob/master/examples/nextjs/pages/_document.js
// https://medium.com/react-courses/speed-up-development-integrate-material-ui-v4-11-0-df7968a43fb6

// CSS prefixing
// https://material-ui.com/getting-started/supported-platforms/
// https://github.com/mui-org/material-ui/blob/47aa5aeaec1d4ac2c08fd0e84277d6b91e497557/pages/_document.js#L123

/* eslint-disable react/jsx-props-no-spreading */

import React from 'react';

import Document, { DocumentContext, DocumentInitialProps } from 'next/document';

// Could not find public export of RenderPage type, so using ReturnType
// deduction instead.
// import { RenderPage } from 'next/dist/next-server/lib/utils';
import { ServerStyleSheet as StyledComponentsSheet } from 'styled-components';

export default class CustomDocument extends Document {
  static async getInitialProps(
    ctx: DocumentContext,
  ): Promise<DocumentInitialProps> {
    const styledComponentsSheet = new StyledComponentsSheet();

    const originalRenderPage = ctx.renderPage;

    try {
      ctx.renderPage = (): ReturnType<typeof ctx.renderPage> => {
        return originalRenderPage({
          enhanceApp: (App) => {
            return (props): JSX.Element => {
              return styledComponentsSheet.collectStyles(<App {...props} />);
            };
          },
        });
      };

      const initialProps = await Document.getInitialProps(ctx);

      return {
        ...initialProps,
        styles: (
          <>
            {initialProps.styles}
            {styledComponentsSheet.getStyleElement()}
          </>
        ),
      };
    } finally {
      styledComponentsSheet.seal();
    }
  }
}
