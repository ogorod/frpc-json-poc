// Hey Emacs, this is -*- coding: utf-8 -*-

import { useState } from 'react';
import type { NextPage } from 'next';

import Head from 'next/head';
// import Image from 'next/image';

import { Grid, Row, Col } from '@zendeskgarden/react-grid';

import { Button } from '@zendeskgarden/react-buttons';

import { SayHello } from '@frpc-json-poc/castle-frpc/dist/esm/ajv/say-hello-client';

import { useAppContext } from '~/lib/front/app-context';

// import styles from '~/styles/Home.module.css';

const Home: NextPage = () => {
  const [state, setState] = useState<{
    helloResults: string[];
  }>({
    helloResults: [],
  });

  const appContext = useAppContext();
  const { castleApi } = appContext;

  return (
    <>
      <Head>
        <title>fRPC JSON POC</title>
        <meta name="description" content="fRPC JSON POC" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
        <Grid>
          <Row>
            <Col>
              <h2>Hello Requester!</h2>
              <Button
                onClick={async (_event): Promise<void> => {
                  const message = await castleApi.ep(SayHello).call({
                    my_name: 'rh',
                    client_id: 'rh-host',
                  });

                  setState((prevState) => {
                    const helloResults = [...prevState.helloResults];
                    helloResults.push(message.hello);

                    return {
                      ...prevState,
                      helloResults,
                    };
                  });
                }}
              >
                Say Hello!
              </Button>
              <div>
                {state.helloResults.map((value, index) => (
                  <p key={index.toString()}>{value}</p>
                ))}
              </div>
            </Col>
          </Row>
        </Grid>
      </main>
    </>
  );
};

export default Home;
