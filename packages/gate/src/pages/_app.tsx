// Hey Emacs, this is -*- coding: utf-8 -*-

/* eslint-disable arrow-body-style */

// import '~/styles/globals.css';

import { useEffect } from 'react';

// import { ThemeProvider as StyledThemeProvider } from 'styled-components';

import { ThemeProvider as ZenThemeProvider } from '@zendeskgarden/react-theming';

import '@zendeskgarden/css-bedrock';

import type { AppProps } from 'next/app';

import { AppContext, AppContextValue } from '~/lib/front/app-context';

const MyApp = ({ Component, pageProps }: AppProps): JSX.Element => {
  const appContextValue = new AppContextValue();

  useEffect(() => {
    const jssStyles = document.querySelector('#jss-server-side');
    if (jssStyles && jssStyles.parentElement) {
      jssStyles.parentElement.removeChild(jssStyles);
    }

    const { castleApi } = appContextValue;

    console.log(`Initial connected = ${castleApi.connected}`);

    castleApi.connected$.subscribe((value) => {
      console.log(`Updated connected = ${value}`);
    });
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <AppContext.Provider value={appContextValue}>
      {/* <StyledThemeProvider theme={theme}> */}
      <ZenThemeProvider>
        <Component {...pageProps} />
      </ZenThemeProvider>
      {/* </StyledThemeProvider> */}
    </AppContext.Provider>
  );
};

export default MyApp;
