/** @type {import('next').NextConfig} */

const withBundleAnalyzer = require('@next/bundle-analyzer')({
  enabled: process.env.ANALYZE === 'true',
});

module.exports = withBundleAnalyzer({
  reactStrictMode: true,

  webpack: (config, { isServer }) => {
    if(!isServer) {
      config.module.rules.push({
        test: /\/lib\/back\//,
        use: 'null-loader',
      });

      config.module.rules.push({
        test: /\/back-.*\//,
        use: 'null-loader',
      });
    }

    return config;
  },
});
