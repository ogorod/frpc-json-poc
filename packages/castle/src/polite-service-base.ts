// Hey Emacs, this is -*- coding: utf-8 -*-

// This file should be auto-codded from fbs file
// describing rpc service

import { TemplatedApp, WebSocket } from 'uWebSockets.js';

/// /b/; Common fRPC imports
/// /b/{

import type {
  FrpcErrorData,
  Packet,
  PacketHeader,
  Request,
} from '@frpc-json-poc/frpc';

import {
  FrpcErrorCode,
  FrpcErrorT,
  frpcErrorMessage,
  requestJtdValidate,
} from '@frpc-json-poc/frpc';

import type {
  SayHelloRequestParams,
  SayHelloResponse,
  SayHelloResponseMessage,
} from '@frpc-json-poc/castle-frpc/dist/cjs/ajv/say-hello-packets';

/// /b/}

/// /b/; say_hello fRPC imports
/// /b/{

import { sayHelloRequestJtdValidate } from '@frpc-json-poc/castle-frpc/dist/cjs/ajv/say-hello-packets';

/// /b/}

type PoliteCallRoute = (
  service: PoliteServiceBase,
  request: Request,
) => Promise<string>;

type PoliteCallRouter = Record<string, PoliteCallRoute>;

const politeCallRouter: PoliteCallRouter = {
  say_hello: async (service, request): Promise<string> => {
    const header: PacketHeader = {
      type: 'response',
    };

    const { id } = request;

    let response: SayHelloResponse;

    if (sayHelloRequestJtdValidate(request)) {
      const { params } = request;
      try {
        const message = await service.sayHello(params);
        response = { header, id, message };
      } catch (e) {
        if (e instanceof FrpcErrorT) {
          const error: FrpcErrorData = {
            code: e.code,
            message: e.message,
          };

          response = { header, id, error };
        } else {
          throw e;
        }
      }
    } else {
      const error: FrpcErrorData = {
        code: FrpcErrorCode.invalid_request,
        message: frpcErrorMessage(FrpcErrorCode.invalid_request),
      };

      response = { header, id, error };
    }

    return JSON.stringify(response);
  },
};

export abstract class PoliteServiceBase {
  readonly router = politeCallRouter;

  constructor(
    public app: TemplatedApp,
    public ws: WebSocket,
    public pingTimeout: ReturnType<typeof setInterval>,
  ) {}

  async received(inData: string): Promise<void> {
    // TODO: Process exceptions

    console.info(`inData = ${inData}`);

    if (this.ws == null) {
      return;
    }

    let packet: Packet | undefined;
    try {
      packet = JSON.parse(inData);
    } catch (error) {
      if (error instanceof SyntaxError) {
        console.warn('Received data is not valid JSON string.');
        return;
      }

      throw error;
    }

    if (!packet?.header?.type) {
      console.warn('Received data object header is invalid.');
      return;
    }

    const packetType = packet.header.type;

    if (packetType === 'request') {
      if (requestJtdValidate(packet)) {
        const method = this.router[packet.method];
        const outData = await method(this, packet);

        console.info(`outData = ${outData}`);

        this.ws.send(outData, false, false);
      }
    } else {
      console.warn(
        [
          `Received packet header type == ${packetType}.`,
          'Only "request" packet types',
          'are supported by this fRPC client.',
        ].join(' '),
      );
    }
  }

  abstract sayHello(
    params: SayHelloRequestParams,
  ): Promise<SayHelloResponseMessage>;
}
