// Hey Emacs, this is -*- coding: utf-8 -*-

/* eslint-disable class-methods-use-this */

import { WebSocket } from 'uWebSockets.js';

/// /b/; Common fRPC imports
/// /b/{

// import type {
//   FrpcError,
// } from '@frpc-json-poc/frpc/dist/cjs/ajv/common';

/// /b/}

/// /b/; say_hello fRPC imports
/// /b/{

import type {
  SayHelloRequestParams,
  SayHelloResponseMessage,
} from '@frpc-json-poc/castle-frpc/dist/cjs/ajv/say-hello-packets';

/// /b/}

/// /b/; Common fRPC imports (to be moved to fRPC library)
/// /b/{

import { RemoveIndexSignature } from '@frpc-json-poc/frpc/dist/cjs/ajv/back';

/// /b/}

/// /b/; Auto-codded fRPC imports
/// /b/{

import { PoliteServiceBase } from './polite-service-base';

/// /b/}

/// /b/; PoliteService Definition
/// /b/{

class PoliteService extends PoliteServiceBase {
  async sayHello(
    params: SayHelloRequestParams,
  ): Promise<SayHelloResponseMessage> {
    const now = new Date();

    const hello = `${now.toISOString()} Hello ${params.my_name} @ ${
      params.client_id
    }`;

    return { hello };
  }
}

interface WebSocketWithPoliteService extends RemoveIndexSignature<WebSocket> {
  service: PoliteService;
}

export { PoliteService as Service };
export type { WebSocketWithPoliteService as WebSocketWithService };

/// /b/}
