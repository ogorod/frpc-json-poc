// Hey Emacs, this is -*- coding: utf-8 -*-

// See for Websockets Authorization
// https://github.com/uNetworking/uWebSockets/issues/966

import { App } from 'uWebSockets.js';

import { Service, WebSocketWithService } from './polite-service';

// const decoder = new StringDecoder('utf8');

const port = 9001;

export const app = App()
  .ws('/*', {
    idleTimeout: 120,
    maxBackpressure: 1024,
    maxPayloadLength: 16 * 1024,

    message: async (ws, message, _isBinary) => {
      const wss = ws as unknown as WebSocketWithService;

      const inData = Buffer.from(message).toString();
      wss.service.received(inData);
    },

    open: (ws) => {
      console.log('****** in open()');

      const wss = ws as unknown as WebSocketWithService;

      wss.service = new Service(
        app,
        ws,
        // TODO: find how to send ping only when connection is idle
        setInterval(() => wss.service.ws?.ping(), 110 * 1000),
      );
    },

    close: (ws, _code, _message): void => {
      console.log('****** in close()');

      const wss = ws as unknown as WebSocketWithService;

      // There should be no app and ws access after close.
      // So let it crash if there is an "after close access" by mistake.

      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      wss.service.app = null!;

      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      wss.service.ws = null!;

      clearInterval(wss.service.pingTimeout);
    },

    // pong: (ws): void => (
    //   console.log('++++++ recieving pong')
    // ),
  })
  .get('/*', (res, _req) => {
    res
      .writeStatus('200 OK')
      .writeHeader('IsExample', 'Yes')
      .end('Hello there!');
  })
  .listen(port, (listenSocket) => {
    if (listenSocket) {
      console.log(`Listening to port ${port}`);
    } else {
      console.log(`Failed to listen to port ${port}`);
    }
  });
