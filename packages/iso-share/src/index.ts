// Hey Emacs, this is -*- coding: utf-8 -*-

export const inBrowser = (): boolean => {
  return typeof window !== 'undefined';
};
